$(document).ready(function(){
	$('#cropbox').Jcrop({
		aspectRatio: cwidth/cheight,
	
		onChange: showCoords,
		onSelect: showCoords,
		//onChange: showPreview,
	});

});

function showCoords(c){
	$('#edit-x1').val(c.x);
	$('#edit-y1').val(c.y);
	$('#edit-x2').val(c.x2);
	$('#edit-y2').val(c.y2);
	$('#edit-w').val(c.w);
	$('#edit-h').val(c.h);
};

